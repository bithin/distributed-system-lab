struct add_in {
    long arg1;
    long arg2;
};

typedef long add_out;

program ADD_PROG {
    version ADD_VERS {
        add_out ADD_PROC(add_in)= 1;
    } = 1;
} = 0x3543000;


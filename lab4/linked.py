"""
This file is part of chord ring structure (Distributed Hash Table).

Chord ring is free program: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Chord ring is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

you should have received a copy of the GNU General Public License
along with Chord ring.  If not, see <http://www.gnu.org/licenses/>.

Written by
[1] Bithin Alangot (PhD scholar, Amrita University)
[2] Seshagiri Prabhu (Master's degree student, Amrita University)

Bug Fixes
[1] Sai Vickneshwar (Local algorithm  expert)

Mentor - our guiding light 
[1] R Vidya (MS in VU)
"""

class Node:
    def __init__(self, data):
        self.data = data
        self.next = None
        self.previous = None
        self.key_list = []
        self.fingertable = {}

class ChordRing:
    def __init__(self):
        self.head = None 
        self.tail = None

    def AddNode(self, data):
        newnode = Node(data)
        temp = self.head

        """
        Initial condition when there's no node in the linked list
        """
        if self.head == None:
            self.head = newnode
            self.tail = newnode
            self.head.next = self.head
            self.head.previous = self.head

        """
        When you are trying to add a node when there's only one node
        """
        if self.head == self.tail:
            self.head.next = newnode
            self.head.previous = newnode
            newnode.next = self.head
            newnode.previous = self.head

            if newnode.data > self.head.data:
                self.tail = newnode

            else:
                self.head = newnode

        #When you are trying to add a node when there's more than one node
        else:

           # If the value of newnode is greater than first node
            if newnode.data > self.head.data:
                """
                If the value of newnode is less than last node
                """
                if newnode.data < self.tail.data:
                    """
                    Traversing till the node which is greater than newnode
                    """
                    while newnode.data > temp.data:
                        if temp.next.data > newnode.data:
                            break
                        temp = temp.next

                    newnode.next = temp.next
                    temp.next = newnode
                    newnode.previous = temp

                # If the value of newnode is greater than tail node
                else:
                    newnode.previous = temp.previous
                    newnode.next = self.tail.next
                    self.tail.next = newnode
                    self.tail = newnode
            # If the value of newnode is lesser than the first node
            else:
                newnode.next = temp
                temp.previous = newnode
                newnode.previous = self.tail
                self.tail.next= newnode
                self.head = newnode

    """
    Adds Data nodes to the Chord
    """
    def AddKey(self, key):
        temp = self.head

        while temp != self.tail:
            if key < temp.data:
                temp.key_list.append(key)
                break
            temp = temp.next
        """
        The keys which are greater than tail node will be appended here
        """
        if temp == self.tail and temp.data > key:
            temp.key_list.append(key)
        """
        The keys whose value lies between head and tail will be added here
        """
        if temp.data < key:
            self.head.key_list.append(key)

    """
    Prints the List
    """
    def PrintList(self):
        node = self.head
        
        print ('Head <--> ') + str(node.data),
        print ('['),
        for t in node.key_list:
            print str(t)+(':'),
        print (']'),

        while node.next != self.head:
            node = node.next
            print ('<--> ') + str(node.data),
            print ('['),
            for t in node.key_list:
                print str(t)+(':'),
            print (']'),

        print ('<--> Tail')

    """
    Prints the content of finger table
    """
    def PrintFingerTable(self):
        node = self.head

        # prints the non tail finger node
        while node.next != self.head:
            print ('Node #') + str(node.data)
            print ('+ + + + + + + + + + ')
            print ('+ Key\t +  Value +')
            print ('+ + + + + + + + + + ')
            for key, value in node.fingertable.iteritems():
                print '+ ' + str(key) + '\t +  ' + str(value.data) + '\t  +'
            print ('+ + + + + + + + + + ')
            node = node.next
        """
        Prints the tail finger node
        """
        print ('Node #') + str(node.data)
        print ('+ + + + + + + + + + ')
        print ('+ Key\t +  Value +')
        print ('+ + + + + + + + + + ')
        for key, value in self.tail.fingertable.iteritems():
            print '+ ' + str(key) + '\t +  ' + str(value.data) + '\t  +'
        print ('+ + + + + + + + + + ')

    """
    Finds the succesor of the given key
    """
    def succesor(self,key):
        temp = self.head
        succ = ""

        """
        Checks if the given key is the head node
        """
        if key == temp.data:
            return temp

        while temp != self.tail:
            """
            Checks if the key is present in the current nodes key list
            """
            if key in temp.key_list:
                succ = temp
                return succ

            else:
                # Checks if the key is node itself
                if key == temp.data:
                    succ = temp
                    return succ

                # Do nothing if the key is not the current node
                else:
                    pass

            temp = temp.next

        if temp == self.tail:
            return self.tail

    """
    Finds the predecessor of the given key. This function is 
    only used while deleting a node.
    """
    def predecessor(self, key):
        temp = self.head
        pred = ""

        """
        Returns the tail node data if the predecessor of the given key is head node
        """
        if key == temp.data:
            return temp.previous.data

        while temp != self.tail:
            """
            If the given key is then in the current node's key list, make the 
            previous node as the predecessor node
            """
            if key in temp.key_list:
                pred = temp.previous.data
                return pred
            else:
                """
                If key equals to the previous node then assigns the predecessor
                as the previous node
                """
                if key == temp.data:
                    pred = temp.previous.data
                    return pred

                # Do nothing
                else:
                    pass
            temp = temp.next

        if temp == self.tail:
            return self.tail.previous.data

    """
    Recursively looks up for a key and discovering the exact path using the finger table
    """
    def lookup_rec(self, node, key, path):
        if key in node.key_list:
            path.append(node.data)
            return path
        else:
            fingertable_length = len(node.fingertable.values())
            for i in range(1,fingertable_length):
                """
                If the key is less than the first entry in the node's finger table\
                """
                if key < node.fingertable.values()[1].data:
                    path.append(node.fingertable.values()[0].data)
                    return self.lookup_rec(node.fingertable.values()[0], key, path)

                else:
                    """
                    If the value of key lies in the values in the fingertable 
                    """
                    if key < node.fingertable.values()[i].data:
                        path.append(node.fingertable.values()[i-1].data)
                        return self.lookup_rec(node.fingertable.values()[i-1], key, path)

                   # If the value of key is greater the final entry in the finger table, it 
                   # assigns the value of the final id in the finger table as the next node
                    elif key > node.fingertable.values()[fingertable_length-1].data and i == fingertable_length-1:
                        path.append(node.fingertable.values()[fingertable_length-1].data)
                        return self.lookup_rec(node.fingertable.values()[fingertable_length-1], key, path)
                   
                   # Do nothing
                    else:
                        pass

    """
    Lookup function to find the shortest path to a given data node
    """
    def lookup(self, key):
        node_path = []
        node_path.append(self.head.data)
        path = self.lookup_rec(self.head, key, node_path)
        return path

    """
    Generates the Finger table of nodes
    """
    def GenerateFingerTable(self):
        temp = self.head
        """
        This generates the finger table for the non tail nodes
        """
        while temp.next != self.head:
            for i in range(1, 6):
                temp.fingertable[i] = self.succesor( (temp.data + 2**(i-1) ) % (2**5) )

            temp = temp.next
        """
        This generates the finger table for the tail node
        """
        if temp == self.tail:
            for i in range(1, 6):
                temp.fingertable[i] = self.succesor( (temp.data + 2**(i-1) ) % (2**5) )

"""
Program starts execution from here
"""
if __name__ == "__main__":
    List = ChordRing()
    """
    Adds the nodes manually to the chord structure
    """
    print "This is a chord ring structure of m = 5"

    Flag = False
    NodeArray = []
    while True:
        print "================================================"
        answer = input('Enter a valid node number. [ 0 - 31]: ')

        """ 
        Sanitization checking
        """
        if answer >= 0 and answer <= 31:         

            """
            Checks if there's any already existing Node with the given node number
            """
            for i in NodeArray:
                if i == answer:
                    Flag = True
                    break

            """
            Adds the new key only if there's no node with the same value
            """
            if Flag == False:
                NodeArray.append(answer)
                List.AddNode(answer)
                List.PrintList()

            else:
                print "You have already entered this node number"
                Flag = False
            print "================================================"
            print ""

        else:
            print "Only enter valid node numbers [ 0 - 31 ]"
            print "================================================"
            print ""
            pass

        cont = raw_input('Would you like to add another node [y/n]: ')
        if cont == 'y':
            continue

        if cont == 'n':
            break

        else:
            print "Enter either 'y' or 'n'!"
            continue

    """
    Adds the keys to the respective nodes
    """
    NodeFlag = False
    KeyFlag = False
    KeyArray = []
    while True:
        print "================================================"
        answer = input ('Enter a valid key [0 - 31]: ')

        """ 
        Sanitization checking
        """
        if answer >= 0 and answer <= 31:
            """
            Checks if there's any already existing Node with the given node number
            """
            for i in NodeArray:
                if i == answer:
                    NodeFlag = True
                    break

            """
            Checks if there's any already existing Key Node with the given key number
            """
            for i in KeyArray:
                if i == answer:
                    KeyFlag = True
                    break

            """
            Adds the new key only if there's no node or key with the same value
            """
            if NodeFlag == False and KeyFlag == False:
                List.AddKey(answer)
                KeyArray.append(answer)
                List.PrintList()

            else:
                if NodeFlag == True:
                    print "There's already a node  with this value. Please try something new next time!"

                if KeyFlag == True:
                    print "Oops! There's already a key with this value. Please input a different key!"

                """
                Assigning False value in-order for the loop to continue
                """
                KeyFlag = False
                NodeFlag = False
            print "===================================================="
            print ""
        else:
            print "Only enter valid key numbers [ 0 - 31 ]"
            print "================================================"
            print ""
            pass

        cont = raw_input('Would you like to add another node [y/n]: ')
        if cont == 'y':
            continue

        if cont == 'n':
            break;

        else:
            print "Enter either 'y' or 'n'!"
            continue

    List.PrintList()
    List.GenerateFingerTable()
    List.PrintFingerTable()

    NodeFlag = False
    KeyFlag = False
    """
    Looks up for data node 26 
    """
    while True:
        reply = raw_input('Would you like to lookup for a value? [y/n]: ')

        if reply == 'y':
            val = input ('Enter the lookup value: ')

            """
            Checks if there's any already existing Node with the given node number
            """
            for i in NodeArray:
                if i == val:
                    NodeFlag = True
                    break
            
            """
            Checks if there's any already existing key with the given node number
            """
            for i in KeyArray:
                if i == val:
                    KeyFlag = True
                    break
            """
            Only looks up if the given input is a key
            """
            if ( KeyFlag ):
                path = List.lookup(val)
                if path:
                    print "========================================"
                    print ('['),
                    for t in List.lookup(val):
                        print str(t) + ('-->'),
                    print ('Key]')
                    print "========================================"
                    print ""
                else:
                    print "Key not found"

            else:
                print "The input which you have provided is not a valid key. Please enter a valid node to lookup"
                print ""

            NodeFlag = False
            KeyFlag = False

        elif reply == 'n':
            print "Exiting from the program. Bye!"
            break

        else:
            print "Only enter either 'y' or 'n;!"
            print ""


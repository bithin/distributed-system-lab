#include "file_add.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char * read_file(char* path){
    FILE *file;
    char *buffer;
    long file_length;

    file = fopen(path, "rb");
    if(file == NULL){
        perror("Unable to open file");
        exit(1);
    }

    file_length = f_length(path);
    printf("%ld", file_length);
    buffer=(char *)malloc(file_length*4);
    if(buffer == NULL){
       perror("Memory Allocation failed");
       exit(1);
    }

    fread(buffer, file_length, 1, file);
    fclose(file);

    return buffer;
}

int f_length(char* path) {
    FILE *fp;
    long file_len; 
    fp = fopen(path, "rb");
    fseek(fp, 0, SEEK_END);
    file_len = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    return file_len;
}

int main(int argc, char** argv){
    CLIENT *cl;
    add_in in;
    add_out *out;
    list_out *l;
    struct info_out *info;
    list_out *temp;
    info_in id;
    fetch_in f_id;
    fetch_out *fetch_details;

    if(argc >= 2) {
        cl = clnt_create(argv[1], FILE_TRANSFER, FILE_VERS, "udp");
        if (strcmp(argv[2],"add") == 0){
            in.author = (char *)malloc(sizeof(char)*strlen(argv[3])+1);
            in.title = (char *)malloc(sizeof(char)*strlen(argv[4])+1);
            strcpy(in.author,argv[3]);
            strcpy(in.title,argv[4]);
            in.file = (struct file_details *)malloc(sizeof(struct file_details));
            in.file->file_val = (char *)malloc(sizeof(char)*f_length(argv[5])+1);
            strcpy(in.file->file_val,read_file(argv[5])); 
            in.file->file_len = f_length(argv[5]);
            out = add_proc_1(&in, cl);
            if(out == NULL) {
                fprintf(stderr,"Error: %s\n", clnt_sperror(cl,argv[1]));
            }
            else {
                printf("%s added", (*out)->f->file_val);
            }
        }
        else if(strcmp(argv[2],"list") == 0) {
            if(out != NULL){
                temp = (list_out *)malloc(sizeof(list_out));
                l = list_proc_1(out, cl);
                for(temp = l; (*temp) != NULL; *temp = (*temp)->next) {
                    printf("%d : %s\n", (*temp)->id, (*temp)->title);
                }
            }
            else {
                printf("No files added to the server");
            }    
        }
        else if(strcmp(argv[2], "info") == 0){
            id = atoi(argv[3]);
            info = info_proc_1(&id, cl);
            printf("%s", *info->author);
            if(info != NULL) {
                printf("The File with ID : %d\n", atoi(argv[3]));
                printf("Author: %s\n", info->author);
                printf("Title: %s\n", info->title);

            }
            else {
                printf("Invalid book ID, try again!");
            }
        }
        else if(strcmp(argv[2], "fetch") == 0){
            f_id = atoi(argv[3]);
            fetch_details = fetch_proc_1(&f_id, cl);
            if(fetch_details != NULL) {
                printf("File data:\n");
                printf("%s", *fetch_details);
            }
        }
        else {
            printf("Command available:\n");
            printf("paperserver <server_name> add <file_name>\n");
            printf("paperserver <server_name> list\n");
            printf("paperserver <server_name> info <file id>\n");
            printf("paperserver <server_name> fetch <file id>\n");
        }                 
    }

    exit(0);
}

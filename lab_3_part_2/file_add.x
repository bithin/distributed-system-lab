struct file_details {
    long int file_len; 
    string file_val<>;
};

struct add_in {
    string author<>;
    string title<>;
    struct file_details *file;
};

struct node {
    int id;
    string author<>;
    string title<>;
    struct file_details *f;
    struct node *next;
};

struct info_out {
    string author<>;
    string title<>;
};

typedef struct node* add_out;
typedef struct node* list_out;
typedef int info_in;
typedef int fetch_in;
typedef char* fetch_out;

program FILE_TRANSFER {
    version FILE_VERS {
        add_out ADD_PROC(add_in) = 1;
        list_out LIST_PROC() = 2;
        info_out INFO_PROC(info_in) = 3;
        fetch_out FETCH_PROC(fetch_in) = 4; 
    }=1;
}=0x3543000;

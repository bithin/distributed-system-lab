#include "file_add.h"

static node *start=NULL;
add_out * 
add_proc_1_svc(add_in *in, struct svc_req *rqstp)
{
    int file_length = in->file->file_len;
    static add_out  result;    
    node *temp, *newfile; 
    newfile = (node *)malloc(sizeof(node));
    newfile->f = (file_details *)malloc(sizeof(file_details));
    newfile->author = (char *)malloc(sizeof(char)*strlen(in->author));
    newfile->title = (char *)malloc(sizeof(char)*strlen(in->title));
    newfile->f->file_val = (char *)malloc(sizeof(char)*file_length);
    strcpy(newfile->author,in->author);
    strcpy(newfile->title,in->title);
    strcpy(newfile->f->file_val,in->file->file_val);
    newfile->f->file_len = file_length;
    newfile->id = rand();
    newfile->next = NULL;    
    if(start == NULL){
       start = newfile;
    }
    else {
        for(temp = start; temp->next != NULL; temp = temp->next);
        temp->next = newfile;
    }
    result = start;
	return &result;
}

list_out *
list_proc_1_svc(void *st, struct svc_req *rqstp)
{
	static list_out  result;
    result = start;
    return &result;
}

info_out *
info_proc_1_svc(info_in *id, struct svc_req *rqstp)
{
	static info_out *result;
    result = (info_out *)malloc(sizeof(info_out));
    node *temp;
    temp = (node *)malloc(sizeof(node));
    for(temp = start; temp->id != *id || temp->next != NULL; temp = temp->next);
    if (temp != NULL){
        result->author = temp->author;
        result->title = temp->title;
    }
	return result;
}

fetch_out *
fetch_proc_1_svc(fetch_in *id, struct svc_req *rqstp)
{
	static fetch_out result;
    node *temp;
    for(temp = start; temp->next != NULL; temp = temp->next) {
        if(temp->id == *id){
            result = (char *)malloc(sizeof(char)*temp->f->file_len);
            result = temp->f->file_val;
        }
    }
	return &result;
}

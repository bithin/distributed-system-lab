/*
 * Please do not edit this file.
 * It was generated using rpcgen.
 */

#include "file_add.h"

bool_t
xdr_file_details (XDR *xdrs, file_details *objp)
{
	register int32_t *buf;

	 if (!xdr_long (xdrs, &objp->file_len))
		 return FALSE;
	 if (!xdr_string (xdrs, &objp->file_val, ~0))
		 return FALSE;
	return TRUE;
}

bool_t
xdr_add_in (XDR *xdrs, add_in *objp)
{
	register int32_t *buf;

	 if (!xdr_string (xdrs, &objp->author, ~0))
		 return FALSE;
	 if (!xdr_string (xdrs, &objp->title, ~0))
		 return FALSE;
	 if (!xdr_pointer (xdrs, (char **)&objp->file, sizeof (file_details), (xdrproc_t) xdr_file_details))
		 return FALSE;
	return TRUE;
}

bool_t
xdr_node (XDR *xdrs, node *objp)
{
	register int32_t *buf;

	 if (!xdr_int (xdrs, &objp->id))
		 return FALSE;
	 if (!xdr_string (xdrs, &objp->author, ~0))
		 return FALSE;
	 if (!xdr_string (xdrs, &objp->title, ~0))
		 return FALSE;
	 if (!xdr_pointer (xdrs, (char **)&objp->f, sizeof (file_details), (xdrproc_t) xdr_file_details))
		 return FALSE;
	 if (!xdr_pointer (xdrs, (char **)&objp->next, sizeof (node), (xdrproc_t) xdr_node))
		 return FALSE;
	return TRUE;
}

bool_t
xdr_info_out (XDR *xdrs, info_out *objp)
{
	register int32_t *buf;

	 if (!xdr_string (xdrs, &objp->author, ~0))
		 return FALSE;
	 if (!xdr_string (xdrs, &objp->title, ~0))
		 return FALSE;
	return TRUE;
}

bool_t
xdr_add_out (XDR *xdrs, add_out *objp)
{
	register int32_t *buf;

	 if (!xdr_pointer (xdrs, (char **)objp, sizeof (node), (xdrproc_t) xdr_node))
		 return FALSE;
	return TRUE;
}

bool_t
xdr_list_out (XDR *xdrs, list_out *objp)
{
	register int32_t *buf;

	 if (!xdr_pointer (xdrs, (char **)objp, sizeof (node), (xdrproc_t) xdr_node))
		 return FALSE;
	return TRUE;
}

bool_t
xdr_info_in (XDR *xdrs, info_in *objp)
{
	register int32_t *buf;

	 if (!xdr_int (xdrs, objp))
		 return FALSE;
	return TRUE;
}

bool_t
xdr_fetch_in (XDR *xdrs, fetch_in *objp)
{
	register int32_t *buf;

	 if (!xdr_int (xdrs, objp))
		 return FALSE;
	return TRUE;
}

bool_t
xdr_fetch_out (XDR *xdrs, fetch_out *objp)
{
	register int32_t *buf;

	 if (!xdr_pointer (xdrs, (char **)objp, sizeof (char), (xdrproc_t) xdr_char))
		 return FALSE;
	return TRUE;
}
